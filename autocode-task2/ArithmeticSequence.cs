﻿using System;

namespace PadawansTask2
{
    public static class ArithmeticSequence
    {
        static void Main(string[] args)
        {
            int number = 2;
            int add = 3;
            int count = 5;
            Calculate(number, add, count);
        }

        public static int Calculate(int number, int add, int count)
        {
            int sum = 0;
            for (int i = 0; i < count; i++)
            {
                sum += number + i * add;
            }
            return sum;
        }
    }
}
